<?php namespace Jondeaves\PostcodeAnywhere;

use Config;
use Jondeaves\PostcodeAnywhere\lib\BankAccountValidation;
use Jondeaves\PostcodeAnywhere\lib\CardValidation;

class PostcodeAnywhere
{

	public function ValidateCard($card_number)
	{

		/*
		 * Remove any spaces
		 */
		$card_number = str_replace(' ', '', $card_number);


		/*
		 * Set up the PostcodeAnywhere API
		 */
		$key = Config::get('postcode-anywhere::keys.payment_validation');
		$pa = new CardValidation($key, $card_number);
		$result = $pa->MakeRequest()->HasData();


		/*
		 * Return false if something is not right at this point, i.e. no data was retrieved
		 */
		if(!$result)
			return ['status' => false, 'description' => 'Those details could not be verified', 'verified' => false];
		else
			$result = $result[0];


		/*
		 * Check for any errors
		 */
		if(isset($result->Error))
		{

			/*
			 * Return error message if it is invalid sort code or account number, otherwise assume error with API not details
			 */
			$valid_errors = ['1001'];
			if(in_array($result->Error, $valid_errors))
				$return = ['status' => false, 'description' => camel_to_words($result->Description), 'verified' => false];
			else
				$return = ['status' => true, 'card_number' => $card_number, 'card_type' => '', 'verified' => false];

		}
		else
		{

			/*
			 * Check value of response
			 */
			if($result->IsValid)
				$return = ['status' => true, 'card_number' => $result->CardNumber, 'card_type' => $result->CardType, 'verified' => true];
			else
				$return = ['status' => false, 'description' => camel_to_words($result->StatusInformation), 'verified' => false];

		}


		/*
		 * Feedback
		 */
		return (object)$return;

	}

	public function ValidateBankAccount($account_number, $sort_code)
	{

		$key = Config::get('postcode-anywhere::keys.payment_validation');
		$pa = new BankAccountValidation($key, $account_number, $sort_code);
		$result = $pa->MakeRequest()->HasData();


		/*
		 * Return false if something is not right at this point, i.e. no data was retrieved
		 */
		if(!$result)
			return ['status' => false, 'description' => 'Those details could not be verified', 'verified' => false];
		else
			$result = $result[0][0];


		/*
		 * Check for any errors
		 */
		if(isset($result->Error))
		{

			/*
			 * Return error message if it is invalid sort code or account number, otherwise assume error with API not details
			 */
			$valid_errors = ['1001', '1002', '1003', '1004'];
			if(in_array($result->Error, $valid_errors))
				$return = ['status' => false, 'description' => camel_to_words($result->Description), 'verified' => false];
			else
				$return = ['status' => true, 'sort_code' => $sort_code, 'account_number' => $account_number, 'verified' => false];

		}
		else
		{

			/*
			 * Check value of response
			 */
			if($result->IsCorrect)
				$return = ['status' => true, 'sort_code' => $result->CorrectedSortCode, 'account_number' => $result->CorrectedAccountNumber, 'verified' => true];
			else
				$return = ['status' => false, 'description' => camel_to_words($result->StatusInformation), 'verified' => false];

		}


		/*
		 * Feedback
		 */
		return (object)$return;

	}

} 