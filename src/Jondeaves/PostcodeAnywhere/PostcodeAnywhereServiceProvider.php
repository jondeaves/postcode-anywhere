<?php namespace Jondeaves\PostcodeAnywhere;

use Illuminate\Support\ServiceProvider;

class PostcodeAnywhereServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('jondeaves/postcode-anywhere');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['postcode-anywhere'] = $this->app->share(function($app)
		{
			return new PostcodeAnywhere;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('postcode-anywhere');
	}

}
