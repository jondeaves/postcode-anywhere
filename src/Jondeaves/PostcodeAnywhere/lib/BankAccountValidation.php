<?php namespace Jondeaves\PostcodeAnywhere\lib;

class BankAccountValidation
{

	/**
	 * The key to use to authenticate to the service.
	 * @var
	 */
	private $Key;

	/**
	 * The bank account number to validate.
	 * @var
	 */
	private $AccountNumber;

	/**
	 * @varThe branch sort code for the account number.
	 */
	private $SortCode;

	/*
	 * Holds the results of the query
	 */
	private $Data;


	function __construct($Key, $AccountNumber, $SortCode)
	{
		$this->Key = $Key;
		$this->AccountNumber = $AccountNumber;
		$this->SortCode = $SortCode;
	}

	function MakeRequest()
	{

		/*
		 * Build request URI
		 */
		$url = "http://services.postcodeanywhere.co.uk/BankAccountValidation/Interactive/Validate/v2.00/json3ex.ws?";
		$url .= "&Key=" . urlencode($this->Key);
		$url .= "&AccountNumber=" . urlencode($this->AccountNumber);
		$url .= "&SortCode=" . urlencode($this->SortCode);


		/*
		 * Make request
		 */
		$client = new \GuzzleHttp\Client();
		$response = json_decode(json_encode($client->get($url)->json()))->Items;


		/*
		 * Build data array
		 */
		$this->Data[] = (array)$response;


		/*
		 * Return $this allowing chaining
		 */
		return $this;

	}

	function HasData()
	{
		if ( !empty($this->Data) )
		{
			return $this->Data;
		}
		return false;
	}

} 