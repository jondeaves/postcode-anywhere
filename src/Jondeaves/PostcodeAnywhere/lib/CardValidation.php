<?php namespace Jondeaves\PostcodeAnywhere\lib;

use Exception;

class CardValidation
{

	//Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.

	private $Key; //The key to use to authenticate to the service.
	private $CardNumber; //The full card number. Any spaces or non numeric characters will be ignored.
	private $Data; //Holds the results of the query

	function __construct($Key, $CardNumber)
	{
		$this->Key = $Key;
		$this->CardNumber = $CardNumber;
	}

	function MakeRequest()
	{
		$url = "http://services.postcodeanywhere.co.uk/CardValidation/Interactive/Validate/v1.00/json.ws?";
		$url .= "&Key=" . urlencode($this->Key);
		$url .= "&CardNumber=" . urlencode($this->CardNumber);


		/*
		 * Make request
		 */
		$client = new \GuzzleHttp\Client();
		$response = json_decode(json_encode($client->get($url)->json()))[0];


		/*
		 * Build data array
		 */
		$this->Data[] = $response;


		/*
		 * Return $this allowing chaining
		 */
		return $this;

	}

	function HasData()
	{
		if ( !empty($this->Data) )
		{
			return $this->Data;
		}
		return false;
	}

}

//Example usage
//-------------
//$pa = new CardValidation_Interactive_Validate_v1_00 ("AA11-AA11-AA11-AA11","4000 1234 1234 1234");
//$pa->MakeRequest();
//if ($pa->HasData())
//{
//   $data = $pa->HasData();
//   foreach ($data as $item)
//   {
//      echo $item["IsValid"] . "<br/>";
//      echo $item["CardNumber"] . "<br/>";
//      echo $item["CardType"] . "<br/>";
//   }
//}