<?php namespace Jondeaves\PostcodeAnywhere;

use Illuminate\Support\Facades\Facade;

class PostcodeAnywhereFacade extends Facade
{

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'postcode-anywhere'; }

} 